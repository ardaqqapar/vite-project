export default {
  testEnvironment: 'jest-environment-jsdom',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    "^.+\\.css$": "<rootDir>/node_modules/jest-css-modules-transform",
  },
  collectCoverageFrom: ['src/**/*.{js,jsx}'],
};