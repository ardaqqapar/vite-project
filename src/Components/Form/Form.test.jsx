import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Form from './Form';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../reduxToolkit/store';

describe('Form', () => {
  test('should return success true when inputting "ardak@gmail.com"', async () => {
    // Set up mock server response
    const serverResponse = { success: true };
    const mockFetch = jest.fn().mockResolvedValueOnce({
      json: jest.fn().mockResolvedValueOnce(serverResponse),
    });
    global.fetch = mockFetch;

    // Render Form component
    render(<Provider store={store}>
        <Form />
      </Provider>);

    // Find input field and enter email
    const inputField = screen.getByLabelText('Email');
    fireEvent.change(inputField, { target: { value: 'ardak@gmail.com' } });

    // Find submit button and click it
    const submitButton = screen.getByText('Subscribe');
    fireEvent.click(submitButton);

    // Wait for the server response to be returned
    await waitFor(() => expect(mockFetch).toHaveBeenCalledTimes(0));

    // Expect server response to be success true
    expect(serverResponse.success).toBe(true);
  });

//   test('should return status 422 when inputting "forbidden@gmail.com"', async () => {
//     // Set up mock server response
//     const mockFetch = jest.fn().mockResolvedValueOnce({
//       status: 422,
//     });
//     global.fetch = mockFetch;

//     // Render Form component
//     render(<Provider store={store}>
//         <Form />
//       </Provider>);

//     // Find input field and enter email
//     const inputField = screen.getByLabelText('Email');
//     fireEvent.change(inputField, { target: { value: 'forbidden@gmail.com' } });

//     // Find submit button and click it
//     const submitButton = screen.getByText('Subscribing...');
//     fireEvent.click(submitButton);

//     // Wait for the server response to be returned
//     await waitFor(() => expect(mockFetch).toHaveBeenCalledTimes(0));

//     // Expect server response status to be 422
//     expect(mockFetch.mock.results[0].value.status).toBe(422);
//   });
});