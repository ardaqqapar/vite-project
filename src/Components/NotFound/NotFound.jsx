import { Link } from 'react-router-dom';
import s from './NotFound.module.css';
import React from 'react';

const NotFound = () => {
  return (
    <div className={s.page}>
      <h1 className={s.header}>Page Not Found</h1>
      <p className={s.text}>Looks like you've followed a broken link or entered a URL that doesn't exist on this </p>
      <Link className={s.link} to="/">←Back to our site</Link>
    </div>
  );
};

export default NotFound;