import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import NotFound from './NotFound';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';


describe('NotFound component', () => {
  it('should render the header, text, and link', () => {
    render(
      <BrowserRouter>
        <NotFound />
      </BrowserRouter>
    );

    expect(screen.getByText('Page Not Found')).toBeInTheDocument();
    expect(screen.getByText("Looks like you've followed a broken link or entered a URL that doesn't exist on this")).toBeInTheDocument();
    expect(screen.getByText('←Back to our site')).toBeInTheDocument();
  });
});
