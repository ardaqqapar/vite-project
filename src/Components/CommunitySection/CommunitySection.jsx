import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { toggleVisibility } from  '../../reduxToolkit/contentSlice'
import { fetchPeople } from '../../reduxToolkit/operations'
import s from './CommunitySection.module.css'
import ContentSection from '../ContentSection/ContentSection'
import { selectContentVisibility, selectPeople } from '../../reduxToolkit/selectors';
import React from 'react';

const CommunitySection = () => {

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchPeople());
      }, [dispatch]);
    const people = useSelector(selectPeople);
    const contentVisible = useSelector(selectContentVisibility);
  
    const handleVisibility = () => {
      dispatch(toggleVisibility())
    }
    
    return (
        <section className={s.section}>
            <h2 className={s.header}>Big Community of People Like You</h2>
            <button className={s.button} onClick={handleVisibility}>{contentVisible ? 'Hide section' : 'Show section'}</button>
            {contentVisible && <ContentSection people={people}/>}           
        </section>
    )
}

export default CommunitySection;