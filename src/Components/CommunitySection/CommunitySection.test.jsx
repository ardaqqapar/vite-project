import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleVisibility } from '../../reduxToolkit/contentSlice';
import { fetchPeople } from '../../reduxToolkit/operations';
import CommunitySection from './CommunitySection';
import { selectContentVisibility, selectPeople } from '../../reduxToolkit/selectors';
import '@testing-library/jest-dom/extend-expect';


jest.mock('react-redux');
jest.mock('../../reduxToolkit/operations');

describe('CommunitySection', () => {
  beforeEach(() => {
    useSelector.mockImplementation((selector) =>
      selector({
        content: {
          visibility: true,
        },
        people: {
          status: 'idle',
          data: [],
        },
      })
    );
    useDispatch.mockReturnValue(jest.fn());
  });

  afterEach(() => {
    useSelector.mockClear();
    useDispatch.mockClear();
  });

  test('renders a header and button', () => {
    render(<CommunitySection />);

    expect(screen.getByRole('heading', { name: /big community/i })).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /show section/i })).toBeInTheDocument();
  });

  test('calls the toggleVisibility action creator when the button is clicked', () => {
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    render(<CommunitySection />);

    fireEvent.click(screen.getByRole('button', { name: /show section/i }));

    expect(dispatch).toHaveBeenCalledWith(toggleVisibility());
  });

  test('renders a ContentSection component when content is visible', () => {
    useSelector.mockImplementation((selector) =>
      selector({
        content: {
          visibility: true,
        },
        people: {
          status: 'succeeded',
          data: [
            { id: 1, name: 'John Doe', age: 30 },
            { id: 2, name: 'Jane Smith', age: 25 },
          ],
        },
      })
    );

    render(<CommunitySection />);

    expect(screen.getByRole('heading', { name: /big community/i })).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /show section/i })).toBeInTheDocument();
  });

  test('does not render a ContentSection component when content is hidden', () => {
    useSelector.mockImplementation((selector) =>
      selector({
        content: {
          visibility: false,
        },
        people: {
          status: 'succeeded',
          data: [
            { id: 1, name: 'John Doe', age: 30 },
            { id: 2, name: 'Jane Smith', age: 25 },
          ],
        },
      })
    );

    render(<CommunitySection />);

    expect(screen.getByRole('heading', { name: /big community/i })).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /show section/i })).toBeInTheDocument();
    expect(screen.queryByRole('table')).not.toBeInTheDocument();
  });

  it('dispatches the fetchPeople action creator on mount', () => {
    const dispatch = jest.fn();
    useDispatch.mockReturnValue(dispatch);

    render(<CommunitySection />);

    expect(fetchPeople).toHaveBeenCalledWith();
    expect(dispatch).toHaveBeenCalledWith(fetchPeople.pending());
  });
});
