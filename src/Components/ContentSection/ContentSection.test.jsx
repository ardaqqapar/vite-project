import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ContentSection from './ContentSection';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import Card from '../Card/Card';
import '@testing-library/jest-dom/extend-expect';


describe('ContentSection', () => {
  const people = [
    { id: 1, name: 'Alice', email: 'alice@example.com' },
    { id: 2, name: 'Bob', email: 'bob@example.com' },
  ];

  it('renders a list of people', () => {
    render(
    <MemoryRouter>
        <ContentSection people={people} />
    </MemoryRouter>    
    );
    const personElements = screen.getAllByRole('img', { name: /Avatar of/ });
    expect(personElements).toHaveLength(2);
  });

  it('renders subtitle', () => {
    render(<MemoryRouter>
        <ContentSection people={people} />
    </MemoryRouter>   );
    const subtitle = screen.getByText('We’re proud of our products, and we’re really excited when we get feedback from our users.');
    expect(subtitle).toBeInTheDocument();
  });
});
