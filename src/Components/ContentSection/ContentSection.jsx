import s from './ContentSection.module.css'
import Card from '../Card/Card';
import { useNavigate } from 'react-router-dom';
import React from 'react';


const ContentSection = ({people}) => {
    const navigate = useNavigate();
    const handleClick = (user) => {
      navigate(`/community/${user.id}`);
    };
    return (
        <>
            <p className={s.subtitle}>We’re proud of our products, and we’re really excited when we get feedback from our users.</p>
            <div className={s.list}>
                {people.map((person)=>(
                    <Card key={person.id} person={person} onClick={()=>handleClick(person)}/>
                ))}
            </div>
        </>
    )
}

export default ContentSection;