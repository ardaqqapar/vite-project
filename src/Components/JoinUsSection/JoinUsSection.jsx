import Form from "../Form/Form";
import s from './JoinUsSection.module.css'
import React from "react"

const JoinUsSection = () => {
    
    return (
        <div className={s.section}>
            <h1>Join Our Program</h1>
            <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <Form data-testid="form-component"/>
        </div>
    )
}

export default JoinUsSection;