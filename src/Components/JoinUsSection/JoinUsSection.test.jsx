import { render, screen } from '@testing-library/react';
import JoinUsSection from './JoinUsSection';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../reduxToolkit/store';
import '@testing-library/jest-dom/extend-expect';


describe('JoinUsSection', () => {
  it('should render the heading and paragraph', () => {
    render(
        <Provider store={store}>
            <JoinUsSection />
        </Provider>)

    const heading = screen.getByRole('heading', { level: 1, name: 'Join Our Program' });
    expect(heading).toBeInTheDocument();

    const paragraph = screen.getByText('Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
    expect(paragraph).toBeInTheDocument();
  });
  it('renders the Form component', () => {
    const { getByTestId } = render(<Provider store={store}>
        <JoinUsSection />
    </Provider>);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
