import { NavLink } from 'react-router-dom'
import React from 'react'
import s from './Navigation.module.css'

const Navigation = () => {
    return (
        <nav className={s.nav}>
            <NavLink className={s.navlink} to='/'>Home</NavLink>
            <NavLink className={s.navlink} to='/community'>Community</NavLink>
        </nav>
    )
}

export default Navigation