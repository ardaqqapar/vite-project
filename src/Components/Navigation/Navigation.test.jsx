import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Navigation from './Navigation';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';


describe('Navigation', () => {
  test('renders two navigation links', () => {
    const { getByText } = render(
      <MemoryRouter>
        <Navigation />
      </MemoryRouter>
    );
    const homeLink = getByText(/home/i);
    const communityLink = getByText(/community/i);
    expect(homeLink).toBeInTheDocument();
    expect(communityLink).toBeInTheDocument();
  });

  test('navigation links have correct href attributes', () => {
    const { getByText } = render(
      <MemoryRouter>
        <Navigation />
      </MemoryRouter>
    );
    const homeLink = getByText(/home/i);
    const communityLink = getByText(/community/i);
    expect(homeLink).toHaveAttribute('href', '/');
    expect(communityLink).toHaveAttribute('href', '/community');
  });
});
