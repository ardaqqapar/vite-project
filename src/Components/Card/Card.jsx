import s from './Card.module.css'
import React from 'react';


const Card = ({ person, onClick }) => {
    
    return(
        <div className={s.card} onClick={onClick} role="card">
            <img src={person.avatar} alt={`Avatar of ${person.firstName}`}/>
            <h2>{person.firstName}</h2>
            <p>{person.position}</p>
        </div>
    )
}
export default Card;