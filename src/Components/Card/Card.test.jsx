import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Card from './Card';
import '@testing-library/jest-dom';

const testPerson = {
  firstName: 'John',
  position: 'Software Developer',
  avatar: 'https://example.com/avatar.jpg',
};

test('Card component renders person information correctly', () => {
  const onClickMock = jest.fn();
  const { getByText, getByAltText } = render(<Card person={testPerson} onClick={onClickMock} />);
  
  const nameElement = getByText(testPerson.firstName);
  const positionElement = getByText(testPerson.position);
  const avatarElement = getByAltText(`Avatar of ${testPerson.firstName}`);

  
  expect(nameElement).toBeInTheDocument();
  expect(positionElement).toBeInTheDocument();
  expect(avatarElement).toBeInTheDocument();
  expect(avatarElement).toHaveAttribute('src', testPerson.avatar);
  
  fireEvent.click(avatarElement);
  expect(onClickMock).toHaveBeenCalledTimes(1);
});