import Card from "../Card/Card"
import { useParams } from 'react-router-dom'
import { useState, useEffect } from "react";
import axios from "axios";
import React from "react";
import { Navigate } from "react-router-dom";

const User = () => {
    let { userId } = useParams();
    const [user, setUser] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const fetchUser = async () => {
            try {
                const response = await axios.get(`http://localhost:3000/community/${userId}`);
                setUser(response.data);
            } catch (error) {
                console.error(error);
            }
            setIsLoading(false);
        };
        fetchUser();
    }, [userId]);
    
    
    return (
        <>
            {isLoading && <div>Loading...</div>}
            {!isLoading && user && <Card person={user} />}
            {!isLoading && !user && <Navigate to="/not-found" />}
        </>
    )
}

export default User