import { getByRole, getByText, render } from '@testing-library/react';
import JoinUsSection from './Components/JoinUsSection/JoinUsSection';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { store } from './reduxToolkit/store';
import { Provider } from 'react-redux';
import App from './App';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import Navigation from './Components/Navigation/Navigation';




describe('JoinUsSection component', () => {
  test('renders join form elements', () => {
    const { getByText, getByPlaceholderText } = render(<Provider store={store}>
        <JoinUsSection />
    </Provider>);
    const headerElement = getByText(/Join Our Program/i);
    const inputElement = getByPlaceholderText(/Email/i);
    const buttonElement = getByText(/Subscribe/i);

    expect(headerElement).toBeInTheDocument();
    expect(inputElement).toBeInTheDocument();
    expect(buttonElement).toBeInTheDocument();
  });
  test('renders routes correctly', () => {
    render(
      <MemoryRouter initialEntries={['/']}>
        <Navigation/>
        <Provider store={store}>
            <App />
        </Provider>
      </MemoryRouter>
    );
    // Navigate to /community and check if the CommunitySection component is rendered
    userEvent.click(screen.getByText(/Community/));
    expect(screen.getByText(/Join Our Program/i)).toBeInTheDocument();
  
  });
});