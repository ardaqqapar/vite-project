import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './reduxToolkit/store';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CommunitySection from './Components/CommunitySection/CommunitySection';
import Navigation from './Components/Navigation/Navigation';
import User from './Components/User/User';
import NotFound from './Components/NotFound/NotFound';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Navigation/>
        <Routes>
          <Route path='/' element={<App />}/>
          <Route path='/community' element={<CommunitySection/>}/>
          <Route path='/community/:userId' element={<User/>}/>  
          <Route path="*" element={<NotFound />} />
          <Route path="/not-found" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);
