import './App.css'
import JoinUsSection from './Components/JoinUsSection/JoinUsSection'
import React from "react"

const App = () => { 
  return (
    <div className="App">
      <JoinUsSection />
    </div>
  )
}

export default App;
 