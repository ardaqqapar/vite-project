export const selectEmail = (state) => state.subscription.email;
export const selectIsSubscribed = (state) => state.subscription.subscribed;
export const selectLoading = (state) => state.subscription.loading;
export const selectError = (state) => state.subscription.error;
export const selectPeople = (state) => state.people && state.people.data;
export const selectContentVisibility = (state) => state.content.visible;