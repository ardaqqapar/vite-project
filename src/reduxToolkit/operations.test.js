import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { fetchPeople, subscribe, unsubscribe } from './operations';

const mockStore = configureStore([thunk]);
const mockAxios = new MockAdapter(axios);

describe('fetchPeople', () => {
  const people = [
    { id: 1, name: 'Alice' },
    { id: 2, name: 'Bob' },
  ];

  afterEach(() => {
    mockAxios.reset();
  });

  it('should dispatch the correct actions on success', async () => {
    mockAxios.onGet('http://localhost:3000/community').reply(200, people);

    const expectedActions = [
      fetchPeople.pending().type,
      fetchPeople.fulfilled(people).type,
    ];

    const store = mockStore();

    await store.dispatch(fetchPeople());

    expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
  });

  it('should dispatch the correct actions on failure', async () => {
    mockAxios.onGet('http://localhost:3000/community').reply(500);

    const expectedActions = [
      fetchPeople.pending().type,
      fetchPeople.rejected().type,
    ];

    const store = mockStore();

    await store.dispatch(fetchPeople());

    expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
  });
});

describe('subscribe', () => {
  const email = 'test@example.com';

  afterEach(() => {
    mockAxios.reset();
  });

  it('should dispatch the correct actions on success', async () => {
    mockAxios.onPost('http://localhost:3000/subscribe').reply(200);

    const expectedActions = [
      subscribe.pending().type,
      subscribe.fulfilled(email).type,
    ];

    const store = mockStore();

    await store.dispatch(subscribe(email));

    expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
  });

  it('should dispatch the correct actions on failure', async () => {
    const errorMessage = 'Email address is invalid';
    mockAxios.onPost('http://localhost:3000/subscribe').reply(400, { error: errorMessage });

    const expectedActions = [
      subscribe.pending().type,
      subscribe.rejected({ message: errorMessage }).type,
    ];

    const store = mockStore();

    await store.dispatch(subscribe(email));

    expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
  });
});

describe('unsubscribe', () => {
    const email = 'test@example.com';
  
    afterEach(() => {
      mockAxios.reset();
    });
  
    it('should dispatch the correct actions on success', async () => {
      mockAxios.onPost('http://localhost:3000/unsubscribe').reply(200);
  
      const expectedActions = [
        unsubscribe.pending().type,
        unsubscribe.fulfilled().type,
      ];
  
      const store = mockStore();
  
      await store.dispatch(unsubscribe(email));
  
      expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
    });
  
    it('should dispatch the correct actions on failure', async () => {
      const errorMessage = 'Email address is invalid';
      mockAxios.onPost('http://localhost:3000/unsubscribe').reply(400, { error: errorMessage });
  
      const expectedActions = [
        unsubscribe.pending().type,
        unsubscribe.rejected({ message: errorMessage }).type,
      ];
  
      const store = mockStore();
  
      await store.dispatch(unsubscribe(email));
  
      expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
    });
  });