import { store } from './store';
import contentReducer from './contentSlice';
import peopleReducer from './peopleSlice';
import subscriptionReducer from './subscriptionSlice'

describe('store', () => {
  it('should combine reducers', () => {
    const state = store.getState();

    expect(state.content).toEqual(contentReducer(undefined, {}));
    expect(state.people).toEqual(peopleReducer(undefined, {}));
    expect(state.subscription).toEqual(subscriptionReducer(undefined, {}));
  });
});