import { configureStore, combineReducers } from '@reduxjs/toolkit';
import contentReducer from './contentSlice';
import peopleReducer from './peopleSlice';
import subscriptionReducer from './subscriptionSlice';

const rootReducer = combineReducers({
  content: contentReducer,
  people: peopleReducer,
  subscription: subscriptionReducer,
});


export const store = configureStore({
  reducer: rootReducer,
});
  