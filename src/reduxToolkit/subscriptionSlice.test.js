import subscriptionReducer, {
    setEmail,
    setSubscribed,
  } from './subscriptionSlice';
  import { subscribe, unsubscribe } from './operations';
  
  describe('subscriptionSlice', () => {
    const initialState = {
      email: '',
      subscribed: false,
      loading: false,
      error: null,
    };
  
    it('should return the initial state on first run', () => {
      expect(subscriptionReducer(undefined, {})).toEqual(initialState);
    });
  
    it('should handle setEmail', () => {
      const email = 'test@test.com';
      const state = subscriptionReducer(initialState, setEmail(email));
      expect(state.email).toEqual(email);
    });
  
    it('should handle setSubscribed', () => {
      const subscribed = true;
      const state = subscriptionReducer(initialState, setSubscribed(subscribed));
      expect(state.subscribed).toEqual(subscribed);
    });
  
    describe('extraReducers', () => {
      it('should handle subscribe.pending', () => {
        const state = subscriptionReducer(initialState, subscribe.pending());
        expect(state.loading).toEqual(true);
        expect(state.error).toEqual(null);
      });
  
      it('should handle subscribe.fulfilled', () => {
        const state = subscriptionReducer(initialState, subscribe.fulfilled());
        expect(state.loading).toEqual(false);
        expect(state.subscribed).toEqual(true);
      });
  
      it('should handle subscribe.rejected', () => {
        const error = undefined;
        const state = subscriptionReducer(
          initialState,
          subscribe.rejected({ error })
        );
        expect(state.loading).toEqual(false);
        expect(state.error).toEqual(error);
      });
  
      it('should handle unsubscribe.pending', () => {
        const state = subscriptionReducer(initialState, unsubscribe.pending());
        expect(state.loading).toEqual(true);
        expect(state.error).toEqual(null);
      });
  
      it('should handle unsubscribe.fulfilled', () => {
        const state = subscriptionReducer(initialState, unsubscribe.fulfilled());
        expect(state.loading).toEqual(false);
        expect(state.subscribed).toEqual(false);
        expect(state.email).toEqual('');
      });
  
      it('should handle unsubscribe.rejected', () => {
        const error = undefined;
        const state = subscriptionReducer(
          initialState,
          unsubscribe.rejected({ error })
        );
        expect(state.loading).toEqual(false);
        expect(state.error).toEqual(error);
      });
    });
  });