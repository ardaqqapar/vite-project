import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { fetchPeople } from './operations';
import peopleReducer from './peopleSlice';

const mockAxios = new MockAdapter(axios);
const mockStore = configureMockStore([thunk]);

describe('peopleSlice', () => {
  describe('fetchPeople', () => {
    afterEach(() => {
      mockAxios.reset();
    });

    it('should dispatch the correct actions on success', async () => {
      const responseData = [{ id: 1, name: 'Alice' }, { id: 2, name: 'Bob' }];
      mockAxios.onGet('http://localhost:3000/community').reply(200, responseData);

      const expectedActions = [
        fetchPeople.pending().type,
        fetchPeople.fulfilled(responseData).type,
      ];

      const store = mockStore();

      await store.dispatch(fetchPeople());

      expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
    });

    it('should dispatch the correct actions on failure', async () => {
      const errorMessage = 'An error occurred';
      mockAxios.onGet('http://localhost:3000/community').reply(500, { message: errorMessage });

      const expectedActions = [
        fetchPeople.pending().type,
        fetchPeople.rejected({ message: errorMessage }).type,
      ];

      const store = mockStore();

      await store.dispatch(fetchPeople());

      expect(store.getActions().map((action) => action.type)).toEqual(expectedActions);
    });
  });

  describe('peopleReducer', () => {
    const initialState = {
      loading: false,
      data: [],
      error: null,
    };

    it('should return the initial state', () => {
      expect(peopleReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle fetchPeople.pending', () => {
      const action = { type: fetchPeople.pending().type };
      const expectedState = { loading: true, data: [], error: null };
      expect(peopleReducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle fetchPeople.fulfilled', () => {
      const responseData = [{ id: 1, name: 'Alice' }, { id: 2, name: 'Bob' }];
      const action = { type: fetchPeople.fulfilled(responseData).type, payload: responseData };
      const expectedState = { loading: false, data: responseData, error: null };
      expect(peopleReducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle fetchPeople.rejected', () => {
      const errorMessage = 'An error occurred';
      const action = { type: fetchPeople.rejected({ message: errorMessage }).type, error: { message: errorMessage } };
      const expectedState = { loading: false, data: [], error: errorMessage };
      expect(peopleReducer(initialState, action)).toEqual(expectedState);
    });
  });
});
