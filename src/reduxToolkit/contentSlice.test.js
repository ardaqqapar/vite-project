import contentReducer, { toggleVisibility } from './contentSlice';

describe('contentSlice', () => {
  describe('toggleVisibility', () => {
    it('should toggle the visibility state from true to false', () => {
      const state = { visible: true };
      const nextState = contentReducer(state, toggleVisibility());
      expect(nextState.visible).toBe(false);
    });

    it('should toggle the visibility state from false to true', () => {
      const state = { visible: false };
      const nextState = contentReducer(state, toggleVisibility());
      expect(nextState.visible).toBe(true);
    });
  });
});